= 4050 Termination of Uncollectible Claims
:chapter-number: 4050
:policy-number: 4050
:policy-title: Termination of Uncollectible Claims
:effective-date: December 2019
:mt: MT-11
:previous-mt: MT-10

include::partial$section-header.adoc[]

== Requirements

Claims are terminated when it is no longer cost-effective to continue collection.

== Basic Considerations

Prior to terminating any claim in Georgia Gateway, screen social security numbers of all liable debtors to ensure that they are not included in another active case that is the same program type as the claim.

Document Georgia Gateway with the reason for the action when claims are terminated manually.

Use the following guidelines for terminating claims in Georgia Gateway:

* Terminate any claim if all liable adult household members are deceased.

* Terminate any claim discharged by bankruptcy unless claim originated via court conviction.

* Terminate any claim if the debt cannot be supported

* Terminate any claim with a current balance of $25 or less if:
** no payment has been made during the past 90 days or more
+
*AND*
** there are no other debts in the same program causing the aggregate debt to be more than $25

* Terminate claims established for 10 or more years if:
** the debtor is not active in another case in the same program
+
*AND*
** no additional liable debtor exists
+
*AND*
** no payment from any source in 5 years